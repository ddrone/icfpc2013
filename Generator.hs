{-# LANGUAGE ScopedTypeVariables #-}

module Generator where

import Control.Monad.State hiding (State)
import Control.DeepSeq (deepseq)
import Debug.Trace (trace)
import System.IO.Unsafe (unsafePerformIO)
import System.IO (hFlush, stdout)
import qualified Data.ByteString.Lazy.Char8 as B
import Network.HTTP
import Network.URI
import Data.Maybe (fromMaybe)
import Control.Monad.Trans.Maybe

import Json
import Datatypes
import Interpreter

type State a = StateT Integer [] a

getString :: Int -> String
getString = ("x" ++) . show

getId :: Int -> Expr
getId = Id . getString

tails [] = []
tails [x] = [(x, [])]
tails (x : xs) =
  let result = tails xs
      (front, tail) = head result
  in (x, front : tail) : result

pairs :: [a] -> [(a, a)]
pairs ls = do
  (x, tl) <- tails ls
  y <- tl
  return (x, y)

-- use n > m, or generation wouldn't work properly
allExpressions :: Int -> Int -> [Expr]
allExpressions _ n | n < 0 = []
allExpressions 0 n = []
allExpressions 1 n = One : Zero : map getId [1..n]
allExpressions m n = case m <= 0 of
  True -> []
  False ->
    let lesserPrograms = map (flip allExpressions n) [0..m - 1]
        getPrograms n = if n < 0 then [] else lesserPrograms !! n
        op1sResults = do
          prog <- getPrograms (m - 1)
          op1 <- allOp1s
          return $ Op1 op1 prog
        op2sResults = do
          s1 <- [1..m - 2]
          let s2 = m - 1 - s1
          p1 <- getPrograms s1
          p2 <- getPrograms s2
          op2 <- allOp2s
          return $ Op2 op2 p1 p2
        ifResults = do
          s1 <- [1..m - 2]
          s2 <- [m - 1 - s1..m - 1]
          let s3 = m - 1 - s1 - s2
          p1 <- getPrograms s1
          p2 <- getPrograms s2
          p3 <- getPrograms s3
          return $ If0 p1 p2 p3
        foldResults = do
          s1 <- [1..m - 3]
          s2 <- [m - 1 - s1..m - 2]
          let s3 = m - 2 - s1 - s2
          p1 <- getPrograms s1
          p2 <- getPrograms s2
          p3 <- getPrograms s3
          (n1, n2) <- pairs $ map getString [1..n]
          return $ Fold p1 p2 $ Func2 (p3, n1, n2)
    in op1sResults ++ op2sResults ++ ifResults ++ foldResults

allPrograms m n = do
  param <- map getString [1..n]
  expr <- allExpressions (m - 1) n
  return $ Func1 (expr, param)

programs = concat $ map (\x -> allPrograms x x) [1..]

passTest :: Value -> Value -> Program -> Bool
passTest input output prog = case evalProgram prog input of
  Left _ -> False
  Right result -> output == result

solve :: [(Value, Value)] -> Program
solve ls = solveConstrained ls programs
  where solveConstrained [] progs = head progs
        solveConstrained ((x, y) : rest) progs =
          solveConstrained rest $ filter (passTest x y) progs

trainRequest = TrainRequest
  { trainRequestSize = Nothing
  , trainRequestOperators = Nothing }

encodeRequest request = prefix ++ request ++ "?auth=" ++ key where
  key = "0151TKgStAEobPAB9GELrcMdRGhwWWAzmA9nY2sDvpsH1H"
  prefix = "http://icfpc2013.cloudapp.net/"

toMaybe :: Either a b -> Maybe b
toMaybe (Left _) = Nothing
toMaybe (Right val) = Just val

prepareRequest ty body =
  let text = B.unpack $ encode body
      url = encodeRequest ty
      uri = fromMaybe (error "URI parsing error") $ parseURI url
  in Request { rqURI = uri
             , rqMethod = POST
             , rqHeaders = [ mkHeader HdrContentType "application/json"
                           , mkHeader HdrContentLength $ show $ length text ]
             , rqBody = text }

-- TODO: using error sucks, need to use monads for error handling
guessLoop :: String -> MaybeT IO ()
guessLoop = guessLoop' programs where
  guessLoop' programs id = do
    let guess = Guess { guessId = id, guessProgram = head programs }
        request = prepareRequest "guess" guess
    lift $ putStrLn $ "current guess" ++ show (guessProgram guess)
    (Response _ _ _ resp) <- MaybeT $ liftM toMaybe $ simpleHTTP request
    let err = error "guess response parsing error"
        guessResult :: GuessResponse = fromMaybe err $ decode $ B.pack resp
    case guessResult of
      GuessResponseWin -> lift $ putStrLn $ "solved instance " ++ id
      GuessResponseMismatch{} -> guessLoop' newPrograms id
        where newPrograms = filter (passTest input output) programs
              input = mismatchInput guessResult
              output = mismatchCorrect guessResult
      GuessResponseError msg -> lift $ putStrLn "ERROR!" >> putStrLn msg

testSolver :: MaybeT IO ()
testSolver = do
  let request = prepareRequest "train" $ trainRequest { trainRequestSize = Just 7 }
  (Response _ _ _ resp) <- MaybeT $ liftM toMaybe $ simpleHTTP request
  let err = error "training problem parsing error"
      problem :: TrainingProblem = fromMaybe err $ decode $ B.pack resp
  lift $ putStrLn $ show problem
  let id = trainingId problem
  lift $ putStrLn id
  guessLoop id

main :: IO ()
main = runMaybeT testSolver >> return ()
