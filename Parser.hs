module Parser where

import Text.ParserCombinators.Parsec hiding (Parser, State)
import Text.Parsec.Pos
import Control.Monad (liftM)
import Control.Monad.State.Lazy
import Data.Char (isSpace)

import Datatypes

op1s = [("not", Not),
        ("shl1", Shl1),
        ("shr1", Shr1),
        ("shr4", Shr4),
        ("shr16", Shr16)]

op2s = [("and", And),
        ("or", Or),
        ("xor", Xor),
        ("plus", Plus)]

data Lexeme = LOpenParen
            | LCloseParen
            | LOther String
            deriving (Show, Eq)

isOk :: Char -> Bool
isOk x = not $ isSpace x || x `elem` "()"

type Token = (SourcePos, Lexeme)
type Parser a = GenParser Token () a

lexer :: String -> State SourcePos [Token]
lexer [] = return []
lexer ('(' : rest) = do
  pos <- get
  put $ updatePosChar pos '('
  liftM ((pos, LOpenParen) :) $ lexer rest
lexer (')' : rest) = do
  pos <- get
  put $ updatePosChar pos ')'
  liftM ((pos, LCloseParen) :) $ lexer rest
lexer ls@(c : rest) =
  if isSpace c
  then do
    pos <- get
    put $ updatePosChar pos c
    lexer rest
  else case span isOk ls of
    (str, rest) -> do
      pos <- get
      put $ updatePosString pos str
      liftM ((pos, LOther str) :) $ lexer rest

etoken :: (Lexeme -> Maybe a) -> Parser a
etoken test = token showToken posToken testToken
  where showToken (pos, tok) = show tok
        posToken (pos, tok) = pos
        testToken (pos, tok) = test tok

identifier :: Parser String
identifier = etoken test
  where test (LOther ident) = return ident
        test _ = Nothing

expect :: String -> Parser ()
expect s = etoken test
  where test (LOther tok) = if tok == s then return () else Nothing
        test _ = Nothing

eatLParen :: Parser ()
eatLParen = etoken test
  where test LOpenParen = return ()
        test _ = Nothing

eatRParen :: Parser ()
eatRParen = etoken test
  where test LCloseParen = return ()
        test _ = Nothing

expression :: Parser Expr
expression = (expect "0" >> return Zero)
             <|> (expect "1" >> return One)
             <|> liftM Id identifier
             <|> do
               eatLParen
               first <- identifier
               result <- case first of
                 "if0" -> do
                   condition <- expression
                   consequence <- expression
                   alternative <- expression
                   return $ If0 condition consequence alternative
                 "fold" -> do
                   vector <- expression
                   initial <- expression
                   eatLParen
                   expect "lambda"
                   eatLParen
                   arg1 <- identifier
                   arg2 <- identifier
                   eatRParen
                   body <- expression
                   eatRParen
                   return $ Fold vector initial $ Func2 (body, arg1, arg2)
                 _ -> case (lookup first op1s, lookup first op2s) of
                   (Just op, _) -> do
                     arg <- expression
                     return $ Op1 op arg
                   (_, Just op) -> do
                     arg1 <- expression
                     arg2 <- expression
                     return $ Op2 op arg1 arg2
                   _ -> fail $ "unexpected symbol " ++ first
               eatRParen
               return result

program :: Parser Program
program = do
  eatLParen
  expect "lambda"
  eatLParen
  arg <- identifier
  eatRParen
  body <- expression
  eatRParen
  return $ Func1 (body, arg)

parseExpression :: String -> String -> Either ParseError Expr
parseExpression src str =
  let (tokens, _) = runState (lexer str) $ initialPos src in
  parse expression src tokens

parseProgram :: String -> String -> Either ParseError Program
parseProgram src str =
  let (tokens, _) = runState (lexer str) $ initialPos src in
  parse program src tokens
