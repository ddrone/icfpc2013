module Interpreter where

import Data.Bits
import Data.List (foldl')
import Data.Foldable (foldrM)

import Datatypes

type Env = [(String, Value)]

type Interpreter a = Either String a

lookupEnv :: Env -> String -> Interpreter Value
lookupEnv env name = case lookup name env of
  Nothing -> Left $ "couldn't find variable " ++ name
  Just value -> return value

unaryOp :: Op1 -> Value -> Value
unaryOp Not x = complement x
unaryOp Shl1 x = shiftL x 1
unaryOp Shr1 x = shiftR x 1
unaryOp Shr4 x = shiftR x 4
unaryOp Shr16 x = shiftR x 16

binaryOp :: Op2 -> Value -> Value -> Value
binaryOp And x y = x .&. y
binaryOp Or x y = x .|. y
binaryOp Xor x y = xor x y
binaryOp Plus x y = x + y

asVector :: Value -> [Value]
asVector v = map proc [0..7]
  where proc i = flip shiftL (sz i + 2) $ v .&. (shiftR mask (sz i))
        sz i = i * 2
        mask = bit 0 .|. bit 1

class EvFuncable a where
  evFunc :: Env -> a -> [Value] -> Interpreter Value

instance EvFuncable Func2 where
  evFunc env (Func2 (e, arg1, arg2)) [v1, v2] = eval newEnv e where
    newEnv = (arg1, v1) : (arg2, v2) : env
  evFunc _ _ ls = Left $ "expected 2 arguments, got " ++ (show $ length ls)

instance EvFuncable Func1 where
  evFunc env (Func1 (e, arg)) [val] = eval newEnv e where
    newEnv = (arg, val) : env
  evFunc _ _ ls = Left $ "expected 1 argument, got " ++ (show $ length ls)

eval :: Env -> Expr -> Interpreter Value
eval env Zero = return 0
eval env One = return 1
eval env (Id id) = lookupEnv env id
eval env expr@If0{} = do
  cond <- eval env $ if0cond expr
  case cond of
    0 -> eval env $ if0then expr
    _ -> eval env $ if0else expr
eval env (Op1 op expr) = do
  value <- eval env expr
  return $ unaryOp op value
eval env (Op2 op e1 e2) = do
  v1 <- eval env e1
  v2 <- eval env e2
  return $ binaryOp op v1 v2
eval env expr@Fold{} = do
  vector <- eval env $ foldVector expr
  init <- eval env $ foldInitial expr
  let proc v1 v2 = evFunc env (foldFunc expr) [v1, v2]
  foldrM proc init (asVector vector)

evalProgram :: Program -> Value -> Interpreter Value
evalProgram prog arg = evFunc [] prog [arg]
