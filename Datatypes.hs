module Datatypes where

import Data.Word (Word64)

type Program = Func1
data Expr = One | Zero | Id String
          | If0 { if0cond :: Expr, if0then :: Expr, if0else :: Expr }
          | Fold { foldVector :: Expr, foldInitial :: Expr, foldFunc :: Func2 }
          | Op1 Op1 Expr
          | Op2 Op2 Expr Expr
          deriving (Show)
data Op1 = Not | Shl1 | Shr1 | Shr4 | Shr16 deriving (Show, Enum, Eq)
data Op2 = And | Or | Xor | Plus deriving (Show, Enum, Eq)
newtype Func1 = Func1 (Expr, String) deriving (Show)
newtype Func2 = Func2 (Expr, String, String) deriving (Show)

allOp1s = enumFrom Not
allOp2s = enumFrom And

type Value = Word64
