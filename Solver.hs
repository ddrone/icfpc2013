{-# LANGUAGE BangPatterns, ScopedTypeVariables #-}
-- module Solver where

import Control.Monad
import Control.Applicative
import Control.Monad.State
import Control.Monad.Error
import Control.Monad.Trans.Maybe
import Control.Concurrent (threadDelay)

import qualified Data.ByteString.Lazy.Char8 as B
import Network.HTTP
import Network.URI
import Data.Maybe (fromMaybe)

import qualified Data.Set as Set
import qualified Data.List as L
import qualified Data.Vector.Unboxed as V
import qualified Data.Set as S

import DeBrujin
import Datatypes
import Json
import qualified Printer as PP

import System.Random

makeSamples :: IO (V.Vector Value)
makeSamples = V.fromList <$> listSamples where
  maxSamplesSize = 256
  listSamples = forM [1..maxSamplesSize] (const randomIO)


encodeRequest request = prefix ++ request ++ "?auth=" ++ key where
  key = "0151TKgStAEobPAB9GELrcMdRGhwWWAzmA9nY2sDvpsH1H"
  prefix = "http://icfpc2013.cloudapp.net/"

prepareRequest body uri =
  let text = encode body
  in Request { rqURI = uri
             , rqMethod = POST
             , rqHeaders = [ mkHeader HdrContentType "application/json"
                           , mkHeader HdrContentLength $ show $ B.length text ]
             , rqBody = text }

type IOThrows = ErrorT String IO
type IOStateThrows s = StateT s (ErrorT String IO)
data Hole = Hole

liftEither (Left err) = throwError err
liftEither (Right val) = return val

extractValue (Right val) = val

toEitherWith val = maybe (Left val) Right

mapLeft f = either (Left . f) Right

makeUri = toEitherWith "URI parse error" . parseURI . encodeRequest

decodeResponse req resp = case decoded of
  Nothing -> Left $ req ++ " response decoding error" ++ B.unpack resp
  Just x -> Right x
  where
    decoded = decode resp
  

serverEval :: String -> (V.Vector Value) -> IOThrows (V.Vector (Value, Value))
serverEval id samples = do
  responseBody <- ErrorT . fmap (fmap rspBody . mapLeft show) . simpleHTTP =<< makeRequest
  evalResp <- liftEither $ decodeResponse "Eval" responseBody
  result <- liftEither . evalResponseToEither $ evalResp
  return (V.zip samples (V.fromList result))
  where
    makeRequest = liftEither req where
      evalRequest = EvalRequest (EvalRequestId id) (V.toList samples)
      req = prepareRequest evalRequest <$> (makeUri "eval")

serverGuess :: String -> IOStateThrows [DeBrujin.Expr] GuessResponse
serverGuess id = do
  program <- getNamedProgram . head <$> get
  liftIO $ putStrLn ("Guess: " ++ PP.print program)
  request <- liftEither . guessRequest . makeGuess $ program
  response <- lift . ErrorT . fmap (mapLeft show) . simpleHTTP $ request
  let responseBody = rspBody $ response
  guessResponse <- liftEither $ decodeResponse "Guess" responseBody
  return guessResponse
  where
    makeGuess prog = Guess { guessId = id,
                           guessProgram = prog }
    guessRequest guess = prepareRequest guess <$> (makeUri "guess")

filterOn samples programs = filter f programs where
  f prog = V.all fit samples where
    fit (input, output) = output == eval [input] prog

stFilterOn samples = filterOn samples <$> get >>= put

solve' :: (V.Vector Value -> IOThrows (V.Vector (Value, Value))) -> -- eval hook
         IOStateThrows s GuessResponse ->                          -- guess hook
         (V.Vector (Value, Value) -> IOStateThrows s ()) ->        -- improve solution
         IOStateThrows s ()
solve' eval guess applySamples = go where
  go = improveResults >> guess >>= fromGuessResponse onSuccess onMismatch onError
  onSuccess = return ()
  onError err = throwError err
  onMismatch (input, output, result) = do
    liftIO $ mapM_ putStr ["Mismatch on input ", show input, " result: ", show result, " (correct: ", show output, ")"]
    liftIO $ putStrLn ""
    applySamples (V.singleton (input, output))
    go
  improveResults = do
    samples <- liftIO makeSamples
    result <- lift $ eval samples
    applySamples result

dumbGetResult = PP.print . getNamedProgram . head . snd

solve id start getResult = extractValue <$> (runErrorT $ go `catchError` handler) where
  handler = liftIO . putStrLn
  go = do
    result <- getResult <$> runStateT action start
    liftIO $ printResult result
    where
      action = solve' (serverEval id) (serverGuess id) stFilterOn
      printResult result = mapM_ putStr ["Result of ", id, ": ", result, "\n"]
  
trainRequest = TrainRequest { trainRequestSize = Just 7
                            , trainRequestOperators = Nothing }

toMaybe :: Either a b -> Maybe b
toMaybe (Left _) = Nothing
toMaybe (Right v) = Just v

ioSolver :: String -> MaybeT IO ()
ioSolver ty = do
  let url = encodeRequest ty
      uri = fromMaybe (error "url parsing error") $ parseURI url
      request = prepareRequest trainRequest uri
  (Response _ _ _ resp) <- MaybeT $ liftM toMaybe $ simpleHTTP request
  -- problem <- liftEither $ decodeResponse "Training problem" resp
  let err = error $ "training problem parsing error: " ++ B.unpack resp
      problem :: TrainingProblem = fromMaybe err $ decode $ resp
  lift $ putStrLn $ show problem
  let progsS = generateTL 10000 (trainingOperators problem) (trainingSize problem) 1
      progs = map fst $ fst $ runState progsS S.empty
      
  lift $ solve (trainingId problem) progs dumbGetResult

testSolver = ioSolver "train"

-- function to solve a particular problem
solveParticular :: String -> [String] -> Int -> IO ()
solveParticular id ops size =
  let progsS = generateTL 10000 ops size 1
      progs = map fst $ fst $ runState progsS S.empty
  in solve id progs dumbGetResult

mix a ls = do
  elem <- ls
  resElem <- [elem, a]
  return resElem

instances = [solveParticular "A1AJFDDjvKqbAoroxXADAqz7" ["and", "shl1"] 6,
             solveParticular "B6nt6FzAjw4523WtFcut6JaS" ["not", "plus", "shr1"] 6,
             solveParticular "DvctNDhHITg9uB6aAZZTko9m" ["or", "shl1"] 6,
             solveParticular "ICvXKlV8jElvxSVx9ExcwIKB" ["shr1", "shr16", "shr4"] 6,
             solveParticular "SygBQH67627lLknuB19uABo2" ["not", "or", "shl1"] 6,
             solveParticular "U1tnhEmIaBCSdBSd57sm9dd7" ["or", "plus"] 6,
             solveParticular "U5P8zslwEhFlw5A2hajFBzD4" ["shr1", "xor"] 6,
             solveParticular "XgAC4oAwLAmYvBmTjwMotU8x" ["plus", "shr1", "shr4"] 6,
             solveParticular "aGaD5HPcggEMqez4CqKwlSfr" ["shl1", "shr1"] 6,
             solveParticular "bJ496C72fA0qv3ienFuCorLt" ["shr4", "xor"] 6,
             solveParticular "dVOSFyLOGXNa9oJ7n8mAKKKD" ["or", "shl1", "shr16"] 6,
             solveParticular "gyHS12SjkoWzuUByjcgvrBeU" ["or", "shl1", "shr16"] 6,
             solveParticular "ibVIn6iV7JnXm2HU8N6T0cG5" ["shr1", "shr4"] 6,
             solveParticular "lPdHfB7CgIIPCkurxuafjbs1" ["shr1", "shr16", "xor"] 6,
             solveParticular "r4r1k65oIZ3AtHzD2vpyKHXW" ["not", "shr1", "xor"] 6,
             solveParticular "rCuiUfP5iNiQsjNAfbJbwuHd" ["plus", "shr1", "shr4"] 6,
             solveParticular "tysPDIApm3C23g3ZBVbRSlyb" ["not", "or"] 6,
             solveParticular "x9aHE3pIIpFIHSdI1S2zeDF7" ["and", "not"] 6,
             solveParticular "3ClhVWLJk9Hhd9xPlxA8RZHg" ["not", "plus", "shr1", "shr16"] 7,
             solveParticular "5LAvFs97vWcWRolR5U7vkiJO" ["plus", "shr16", "xor"] 7,
             solveParticular "658BvtmUw2FFJAN7zjUzh8M1" ["and", "not", "or"] 7,
             solveParticular "9MpIPaZCYHB7MN5gYAEpOsqp" ["and", "shr4", "xor"] 7,
             solveParticular "C00sBk4d6uwCKPPU3PIdUKzX" ["or", "plus", "shr4"] 7]

solveEasyInstances :: IO ()
solveEasyInstances = sequence_ $ mix (threadDelay 20000000) instances

main :: IO ()
main = runMaybeT testSolver >> return ()