{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, TypeSynonymInstances #-}
module Json (
  Seconds
  , Problem (..)
  
  , EvalRequestIdentifier (..)
  , EvalRequest (..)
  , EvalResponse (..)
  , evalResponseToEither

  , Guess (..)
  , GuessResponse (..)
  , fromGuessResponse

  , TrainRequest (..)
  , TrainingProblem (..)

  , Window (..)
  , Status (..)

  , encode
  , decode
) where

import Control.Applicative
import Control.Monad

import qualified Data.Text as Text
import Data.Char
import Data.Maybe
import Numeric
import qualified Data.Vector as V

import Data.Aeson (encode, decode)
import Data.Aeson (ToJSON(..), FromJSON(..), (.:), (.:?), (.=), object)
import Data.Aeson.Types hiding (Value) 
import qualified Data.Aeson.TH as AT
import qualified Data.ByteString.Lazy.Char8 as B

import Datatypes (Program, Value)
import qualified Printer as PP (print)
import Parser (parseProgram)

get v str = v .: Text.pack str
mayGet v str = v .:? Text.pack str

fromJsonInt :: String -> Value
fromJsonInt = fst . head .  readHex . drop 2

toJsonInt :: Value -> String
toJsonInt x = (prefix . showHex x) ""
  where prefix = showString "0x"

type Seconds = Double

instance FromJSON Program where
  parseJSON (String s) = case parseProgram "" (Text.unpack s) of
    Left error -> mzero
    Right prog -> return prog
  parseJSON _ = mzero

instance ToJSON Program where
  toJSON = String . Text.pack . PP.print

data Problem = Problem
               { problemId :: String
               , problemSize :: Int
               , problemOperators :: [String]
               , problemSolved :: Bool
               , timeLeft :: Seconds
               } deriving (Show)

instance FromJSON Problem where
  parseJSON (Object v) = Problem <$>
                         get v "id" <*>
                         get v "size" <*>
                         get v "operators" <*>
                         (fromMaybe False <$> mayGet v "solved") <*>
                         (fromMaybe 300 <$> mayGet v "timeLeft")
  parseJSON _ = mzero


decProblem :: Maybe Problem
decProblem = decode (B.pack "{ \"id\": \"foobar\", \"size\": 30, \"operators\": [\"shl\", \"fold\"], \"solved\": true }")


data EvalRequestIdentifier = EvalRequestId String 
                           | EvalRequestProgram Program
                           deriving (Show)

data EvalRequest = EvalRequest
                   { requestIdentifier :: EvalRequestIdentifier
                   , requestArguments :: [Value]
                   } deriving (Show)

instance ToJSON EvalRequest where
  toJSON (EvalRequest identifier arguments) = object [ Text.pack "arguments" .= encodedArguments, makePair identifier ]
    where makePair (EvalRequestId id) = Text.pack "id" .= id
          makePair (EvalRequestProgram prog) = Text.pack "program" .= prog
          encodedArguments = map toJsonInt arguments

data EvalResponse = EvalResponseError { evalErrorMessage :: String }
                  | EvalResponseOK { evalResult :: [Value] }
                    deriving (Show)

evalResponseToEither (EvalResponseError e) = Left e
evalResponseToEither (EvalResponseOK result) = Right result

instance FromJSON EvalResponse where
  parseJSON (Object v) = do
    status <- get v "status"
    case status == "ok" of
      True -> do
        outputs <- v .: Text.pack "outputs"
        return $ EvalResponseOK (map fromJsonInt outputs)
      False -> do
        EvalResponseError <$> v .: Text.pack "message"      
  parseJSON _ = mzero

evalResponses = map B.pack
  [ "{ \"status\": \"ok\", \"outputs\": [\"0xffffffffffffffff\", \"0x34\", \"0x44\"] }"
  , "{ \"status\": \"error\", \"message\": \"just kidding\" }"
  ]
decEvalResponses :: [Maybe EvalResponse]
decEvalResponses = map decode evalResponses

data Guess = Guess
             { guessId :: String
             , guessProgram :: Program
             } deriving (Show)
$(AT.deriveJSON (map toLower . drop (length "guess")) ''Guess)

data GuessResponse = GuessResponseWin
                   | GuessResponseMismatch { mismatchInput :: Value,
                                             mismatchCorrect :: Value,
                                             mismatchResult :: Value }
                   | GuessResponseError String
                   deriving (Show)

fromGuessResponse w m e r = case r of
  GuessResponseWin -> w
  GuessResponseMismatch input correct result -> m (input, correct, result)
  GuessResponseError err -> e err

instance FromJSON GuessResponse where
  parseJSON (Object v) = do
    status <- get v "status"
    case status of
      "win" -> return GuessResponseWin
      "mismatch" -> do
        [arg, correct, our] <- map fromJsonInt <$> get v "values"
        return $ GuessResponseMismatch arg correct our
      "error" -> do
        GuessResponseError <$> get v "message"
  parseJSON _ = mzero

guessResponses = map B.pack
  [ "{ \"status\": \"win\" }"
  , "{ \"status\": \"mismatch\", \"values\" : [\"0x10\", \"0x5\", \"0x4\"] }"
  , "{ \"status\": \"error\", \"message\": \"incorrect problem id\" }" ]

decGuessResponses :: [Maybe GuessResponse]
decGuessResponses = map decode guessResponses

data TrainRequest = TrainRequest
                    { trainRequestSize :: Maybe Int
                    , trainRequestOperators :: Maybe [String]
                    } deriving (Show)
instance ToJSON TrainRequest where
  toJSON (TrainRequest size operators) = object (getNullable size "size" ++
                                                 getNullable operators "operators")
    where
      getNullable Nothing _ = []
      getNullable (Just x) field = [Text.pack field .= x]


data TrainingProblem = TrainingProblem
                       { trainingChallenge :: Program
                       , trainingId :: String
                       , trainingSize :: Int
                       , trainingOperators :: [String]
                       } deriving (Show)
$(AT.deriveJSON (map toLower . drop (length "training")) ''TrainingProblem)

decTrainProblem :: Maybe TrainingProblem
decTrainProblem = decode $ B.pack "{ \"challenge\": \"(lambda (x) (if0 x (shl1 x) (and x 1)))\", \"id\": \"foobar\", \"size\": 20, \"operators\": [\"if0\", \"shl1\", \"and\" ] }"

data Window = Window
                  { resetsIn :: Seconds
                  , amount :: Int
                  , limit :: Int
                  } deriving (Show)
$(AT.deriveJSON id ''Window)

data Status = Status
              { easyChairId :: Int
              , contestScore :: Int
              , lightningScore :: Int
              , trainingScore :: Int
              , mismatches :: Int
              , numRequests :: Int
              , requestWindow :: Window
              , cpuWindow :: Window
              , cpuTotalTime :: Int
              }
$(AT.deriveJSON id ''Status)
