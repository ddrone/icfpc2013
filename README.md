# ICFPC 2013 submission by team ITMonad

The solution is quite simple: generate a list of all expressions with size less
than or equals to given, then make a eval request to get input-output pairs,
filter out all expressions that evaluate to different output on at least one of
given inputs, and then take first of filetered list and submit it to the
server.

We've tried to come up with different solution, but failed to do so, and
haven't tried very hard to optimize solution that we've got.

Our main fault was that we've missed the following line from the problem
description: «A valid program P contains at most one occurrence of
"fold"». Because there were much more programs with many occurrences of
"fold" than with at most one occurrence, our solution failed to find examples
in appropriate time to much smaller expressions than properly written
bruteforce solution would.

Given that in proper expression with many occurrences of "fold" there could be
many different variables, we've generated named representations at first, but
then switched to generating terms with de Brujin indices. When generating
expressions in unnamed representation, we decided to generate them not in
order of increasing size, but in order of increasing depth to simplify code a
little bit. Seems like it was another flaw in our solution.

So, we've finished with only 61 points, but still had fun participating in the
contest.
