module Printer where

import Prelude hiding (print)
import Data.List (intercalate)

import Datatypes

class Printable a where
  print :: a -> String

wrap str = "(" ++ str ++ ")"

instance Printable Expr where
  print Zero = "0"
  print One = "1"
  print (Id str) = str
  print (If0 e1 e2 e3) =
    wrap $ intercalate " " ["if0", print e1, print e2, print e3]
  print (Fold e1 e2 e3) =
    wrap $ intercalate " " ["fold", print e1, print e2, print e3]
  print (Op1 op e) =
    wrap $ intercalate " " [print op, print e]
  print (Op2 op e1 e2) =
    wrap $ intercalate " " [print op, print e1, print e2]

instance Printable Op1 where
  print Not = "not"
  print Shl1 = "shl1"
  print Shr1 = "shr1"
  print Shr4 = "shr4"
  print Shr16 = "shr16"

instance Printable Op2 where
  print And = "and"
  print Or = "or"
  print Xor = "xor"
  print Plus = "plus"

instance Printable Func1 where
  print (Func1 (e, arg)) =
    wrap $ intercalate " " ["lambda", wrap arg, print e]

instance Printable Func2 where
  print (Func2 (e, a1, a2)) =
    wrap $ intercalate " " ["lambda", wrap $ intercalate " " [a1, a2], print e]