module Test where

import Control.Monad
import Control.Applicative ((<$>), (<*>))
import Network.HTTP
import Network.URI
import Data.Maybe (fromMaybe)
import Data.Int (Int64)
import Data.Bits

import qualified Data.ByteString.Lazy.Char8 as B

import Json
import Parser
import Datatypes

url = "http://icfpc2013.cloudapp.net/eval?auth=0151TKgStAEobPAB9GELrcMdRGhwWWAzmA9nY2sDvpsH1H"
jsonType = "application/json"
-- testBody = "{\"program\":\"(lambda (x) (shr16 x))\",\"arguments\":[\"0xFF00000000000000\"]}"

uri = fromMaybe (error "parse error") $ parseURI url
guess = Guess { guessId = "foobar", guessProgram=Func1 (Id "x", "x") }

makeRequest request = Request
  { rqURI = uri
  , rqMethod = POST
  , rqHeaders = [ mkHeader HdrContentType jsonType
                , mkHeader HdrContentLength $ show $ length body ]
  , rqBody = body }
  where
    body = B.unpack $ encode request

readAndEval = do
  putStr "> "
  prog <- parseProgram "" <$> getLine
  case prog of
    Left err -> error ("parse error" ++ show err)
    Right prog -> do
      putStr "? "
      args <- map read . words <$> getLine
      result <- simpleHTTP (makeRequest $ evalProgram prog args)
      case result of
        Left err -> putStrLn "connection error"
        Right response -> putStrLn $ rspBody response

main :: IO ()
main = do
  readAndEval
  main
  
