{-# LANGUAGE ScopedTypeVariables #-}

module DeBrujin where

import Control.Monad (sequence, liftM, liftM2, liftM3)
import Control.Monad.State.Lazy
import Data.Set (Set)
import qualified Data.Set as S
import Control.Applicative ((<$>), (<*>), pure, liftA2, liftA3)
import Control.Monad (join)
import Data.List (foldl', elemIndex)
import Data.Maybe (fromJust, mapMaybe)

import Parser (op1s, op2s)
import Datatypes hiding (Expr(..))
import qualified Datatypes as D (Expr(..))
import Interpreter (unaryOp, binaryOp, asVector)

data Expr = One | Zero | Id Int
          | If0 Expr Expr Expr -- cond then else
          | Fold Expr Expr Expr -- vector init body
          | Op1 Op1 Expr
          | Op2 Op2 Expr Expr
          deriving (Show, Eq)

getVariable :: Int -> State ([String], Integer) String
getVariable n = do
  (stack, _) <- get
  return $ stack !! n

fresh :: State (a, Integer) String
fresh = do
  (stack, n) <- get
  put (stack, n + 1)
  return $ "x" ++ show n

putOnStack :: [a] -> State ([a], b) ()
putOnStack ls = do
  (stack, x) <- get
  put (ls ++ stack, x)

popFromStack :: Int -> State ([a], b) ()
popFromStack n = do
  (stack, x) <- get
  put (drop n stack, x)

toNamed :: Expr -> State ([String], Integer) D.Expr
toNamed One = return D.One
toNamed Zero = return D.Zero
toNamed (Id n) = liftM D.Id $ getVariable n
toNamed (If0 e1 e2 e3) = do
  e1' <- toNamed e1
  e2' <- toNamed e2
  e3' <- toNamed e3
  return $ D.If0 e1' e2' e3'
toNamed (Fold e1 e2 e3) = do
  e1' <- toNamed e1
  e2' <- toNamed e2
  p1 <- fresh
  p2 <- fresh
  putOnStack [p1, p2]
  e3' <- toNamed e3
  popFromStack 2
  return $ D.Fold e1' e2' (Func2 (e3', p1, p2))
toNamed (Op1 op e) = liftM (D.Op1 op) $ toNamed e
toNamed (Op2 op e1 e2) = liftM2 (D.Op2 op) (toNamed e1) (toNamed e2)

getNamed :: Expr -> D.Expr
getNamed expr = fst $ runState (toNamed expr) ([], 0)

getNamedProgram :: Expr -> Program
getNamedProgram expr =
  let name = "arg"
      result = fst $ runState (toNamed expr) ([name], 0)
  in Func1 (result, name)

toDeBrujin :: D.Expr -> State [String] Expr
toDeBrujin D.One = return One
toDeBrujin D.Zero = return Zero
toDeBrujin (D.Id s) = get >>= return . Id . fromJust . elemIndex s
toDeBrujin (D.If0 e1 e2 e3) =
  liftM3 If0 (toDeBrujin e1) (toDeBrujin e2) (toDeBrujin e3)
toDeBrujin (D.Op1 op e1) = liftM (Op1 op) $ toDeBrujin e1
toDeBrujin (D.Op2 op e1 e2) =
  liftM2 (Op2 op) (toDeBrujin e1) (toDeBrujin e2)
toDeBrujin (D.Fold e1 e2 (Func2 (e3, p1, p2))) = do
  e1' <- toDeBrujin e1
  e2' <- toDeBrujin e2
  stack <- get
  put $ [p1, p2] ++ stack
  e3' <- toDeBrujin e3
  put stack
  return $ Fold e1' e2' e3'

getDeBrujin :: Program -> Expr
getDeBrujin (Func1 (body, arg)) = fst $ runState (toDeBrujin body) [arg]

-- when executing fold expressions, we push vector and initial on the execution
-- stack, thus lambda bindings are implicit in this interpreter, be careful!

-- this interpreter focused on speed, so it uses unsafe list indexing function
eval :: [Value] -> Expr -> Value
eval _ Zero = 0
eval _ One = 1
eval stack (Id id) = stack !! id
eval stack (If0 cond conseq alt) =
  case eval stack cond of
    0 -> eval stack conseq
    _ -> eval stack alt
eval stack (Fold vector init body) =
  let elems = asVector $ eval stack vector
      initVal = eval stack init 
      proc elem value = eval (elem : value : stack) body
  in foldr proc initVal elems
eval stack (Op1 op1 expr) = unaryOp op1 $ eval stack expr
eval stack (Op2 op2 e1 e2) =
  binaryOp op2 (eval stack e1) (eval stack e2)

type Values = Set Value

mapBoth f g (x, y) = (f x, g y)

type Ternop a = a -> a -> a -> a
combine3 :: Ternop a -> Ternop b -> Ternop (a, b)
combine3 f g (x, a) (y, b) (z, c) = (f x y z, g a b c)

const3 x _ _ _ = x

evalConstIf 0 x y = x
evalConstIf _ x y = y

-- TODO: carefully check that this interpretation is correct
-- same thing for Fold case of eval function
evalConstFold els initVal expr =
  if not $ isClosed 2 expr
  then Nothing
  else let elems = asVector els
           proc elem value = eval [elem, value] expr
       in return $ foldr proc initVal elems

isClosed :: Int -> Expr -> Bool
isClosed _ One = True
isClosed _ Zero = True
isClosed l (Id n) = n < l
isClosed l (If0 e1 e2 e3) =
  foldl' (&&) True $ map (isClosed l) [e1, e2, e3]
isClosed l (Fold e1 e2 e3) =
  isClosed l e1 && isClosed l e2 && isClosed (l + 2) e3
isClosed l (Op1 _ e) = isClosed l e
isClosed l (Op2 _ e1 e2) = isClosed l e1 && isClosed l e2

joinT :: StateT s [] [a] -> StateT s [] a
joinT action = do
  a <- action
  r <- lift $ a
  return r

-- yield lifts a value into a monad only if there wasn't already an
-- expression with the same value
-- TODO: maybe yield is not a good name for this function
yield :: (Expr, Maybe Value) -> StateT Values [] (Expr, Maybe Value)
yield p@(_, Nothing) = lift [p]
yield p@(_, Just v) = do
  values <- get
  case S.member v values of
    True -> lift $ []
    False -> do
      put $ S.insert v values
      return p

yieldS :: Int -> (Expr, Maybe Value) -> State Values [(Expr, Maybe Value)]
yieldS _ p@(_, Nothing) = return [p]
yieldS limit p@(_, Just v) = do
  values <- get
  case S.member v values of
    True -> return []
    False -> case S.size values < limit of
      True -> put (S.insert v values) >> return [p]
      False -> return [p]

yieldL :: Int -> [(Expr, Maybe Value)] -> State Values [(Expr, Maybe Value)]
yieldL limit ls = foldM (\a -> liftM (a ++) . yieldS limit) [] ls

generateTL :: Int -> [String] -> Int -> Int -> State Values [(Expr, Maybe Value)]
generateTL limit ops = generate limit doFolds o1s o2s doTFolds
  where doFolds = "fold" `elem` ops
        doTFolds = "ftold" `elem` ops
        o1s = mapMaybe (flip lookup op1s) ops
        o2s = mapMaybe (flip lookup op2s) ops

-- generating by depth, not by size, this allows to write simpler code
generate :: Int -> Bool -> [Op1] -> [Op2] -> Bool -> Int -> Int -> State Values [(Expr, Maybe Value)]
generate limit doFolds usedOp1s usedOp2s = generate' where
  generate' _ 0 _ = return []
  generate' _ 1 stackSize = do
    l1 <- yieldS limit (One, Just 1)
    l2 <- yieldS limit (Zero, Just 0)
    return $ l1 ++ l2 ++ map (flip (,) Nothing . Id) [0..stackSize - 1]
  generate' doTFolds exprSize stackSize | exprSize > 0 && stackSize >= 0 = do
    elems <- generate' False (exprSize - 1) stackSize
    foldBodies <- generate' False (exprSize - 1) (stackSize + 2)
    let ifs = do
          cond <- elems
          conseq <- elems
          alt <- elems
          return $ combine3 If0 (liftA3 evalConstIf) cond conseq alt
        folds = do
          (vector, vVal) <- elems
          (init, iVal) <- elems
          (body, _) <- elems ++ foldBodies
          let val = evalConstFold <$> vVal <*> iVal <*> pure body
          return (Fold vector init body, join val)
        op1s = do
          op <- usedOp1s
          (arg1, aval) <- elems
          return (Op1 op arg1, unaryOp op <$> aval)
        op2s = do
          op <- usedOp2s
          (arg1, v1) <- elems
          (arg2, v2) <- elems
          return (Op2 op arg1 arg2, binaryOp op <$> v1 <*> v2)
        ls = case (doFolds, doTFolds) of
          (_, True) -> [folds]
          (True, _) -> [op1s, op2s, ifs, folds]
          _ -> [op1s, op2s, ifs]
    liftM ((elems ++) . concat) $ sequence $ map (yieldL limit) ls
