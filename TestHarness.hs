module TestHarness where

import DeBrujin
import Control.Monad.State.Lazy
import qualified Data.Set as S
import Datatypes (allOp1s, allOp2s)

testProperty :: Show a => (a -> Bool) -> [a] -> IO ()
testProperty _ [] = putStrLn "all tests passed"
testProperty p (x : xs) = do
  putStrLn $ "testing: " ++ show x
  case p x of
    False -> putStrLn "TEST FAILED!!"
    True -> testProperty p xs

prop_inverse x = x == (getDeBrujin $ getNamedProgram x)
test_inverse n count = testProperty prop_inverse $ map fst $ take count $ ls
  where ls = fst $ runState (generate 100 True allOp1s allOp2s n 1) S.empty